import { Component } from '@angular/core';
import { CountriesComponent } from '../countries/countries.component';

@Component({
  selector: 'app-long-weekends',
  templateUrl: './long-weekends.component.html',
  styleUrls: ['./long-weekends.component.css']
})
export class LongWeekendsComponent extends CountriesComponent {

  events(){
  this.getLongWeekends(this.actualYear, this.selectedValue);
}

}