import { Component } from '@angular/core';
import { CalendarComponent } from '../calendar/calendar.component';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent extends CalendarComponent {
  
  countries:any[]
  countryInfo:any = {}
  selectedValue: string = '';
  choose: string = "choose a country"
  value:string = 'countryCode'
  name:string = 'name'
  

  buttonDisabled : boolean = true;



  //cargo primero la vista para evitar errores
  ngAfterViewInit(): void {
    this.getCountries() 
    
 }

   onSelectedValueChange(value: string) {
    this.selectedValue = value;

    this.buttonDisabled = value === this.choose ? true : false;

  // Utiliza el valor seleccionado en el componente receptor
}



 handleButtonClick() {

  this.showSpinner = true;

  if(this.ifLongweekends(this.route)){
    this.getLongWeekends(this.actualYear, this.selectedValue);


  }
  else{
 this.getPublicHolidays(this.actualYear, this.selectedValue );

  }
//  this.getCountryInfo()
    
}

  getCountries(){
    this.countriesPovider.getCountries()
    .then(countries=>{
      this.countries = countries.data
    })
  }

  getCountryInfo() {
    this.countriesPovider.getCountryInfo(this.selectedValue)
    .then(info => {
      this.countryInfo = info;
    })
    .catch(error=>{
      console.log(error)
    });
    
  }




}



  

