import { Component } from '@angular/core';
import { CountriesComponent } from '../countries/countries.component';

@Component({
  selector: 'app-public-holidays',
  templateUrl: './public-holidays.component.html',
  styleUrls: ['./public-holidays.component.css']
})
export class PublicHolidaysComponent extends CountriesComponent {

events(){
  this.getPublicHolidays(this.actualYear, this.selectedValue );
}

}
