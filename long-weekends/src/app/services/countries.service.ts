import { Injectable } from '@angular/core';
import { restService } from './restService';

@Injectable({
  providedIn: 'root'
})
export class CountriesService extends restService {

getCountries() {
  const endpoint = "AvailableCountries";
  this.clearCache()
  return this.get(endpoint); 
}

getCountryInfo(countryCode: string) {
  const endpoint = `CountryInfo/${countryCode}`;
  return this.get(endpoint);
}

}
