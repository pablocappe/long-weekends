import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class restService {

  //Armar un metodo que me traiga la URL exect_get, guardar la URL en enviroment.-
    private baseURL = environment.baseURL;
    private responseCache = new Map<string, any>();

  
    constructor(private http: HttpClient) { }
  
    get(endpoint: string) {
      const url = `${this.baseURL}${endpoint}`;
      
      if (this.responseCache.has(url)) {
        return Promise.resolve(this.responseCache.get(url));
      }
    
      return axios.get(url)
        .then(response => {
          // Almacenar la respuesta en la caché
          this.responseCache.set(url, response);
          return response;
        });
    }

    clearCache() {
      this.responseCache.clear();
    }
  }