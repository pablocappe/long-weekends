import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'pc-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.css']
})
export class LinkComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input()
  linkHref:string="";

  @Input()
  linkTitle:string="";

  @Input()
  class:string=""

  @Input()
  linkText:string="";

  @Input() activeClass: string;

  

}
