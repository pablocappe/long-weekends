import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'pc-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent {

  constructor() { }

  @Input()
  class:string = 'primary'


}
